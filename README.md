# ai bot
it doesn't use phind anymore, i just did not rename the repo.
it uses a random website i found on github to talk to gpt-3.5

this is a fork of [techlore's faq bot](https://github.com/techlore/faq-bot).

# how use?
1. `pip3 install matrix-nio aiohttp`

2. create login.json:
```json
{
	"homeserver":"https://homeserver",
	"username":"@user:server",
	"token": "matrix token"
}
```

3. start main.py.

note: you probably want to change `userId` in main.py to something else, or you
will be using the same chat history as me
