import asyncio
from nio import (AsyncClient, RoomMessageText)
import json
import aiohttp
import re

with open('login.json') as loginfile:
    logindata = json.load(loginfile)

client = AsyncClient(logindata["homeserver"], logindata["username"])
session = None

async def post(url, json):
    """Make a post request without the indentation stuff"""
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0",
        "Content-Type": "application/json",
        "Accept": "application/json, text/plain, */*",
        "Origin": "https://chat9.yqcloud.top",
        "Referer": "https://chat9.yqcloud.top/"
    }
    print("post request")
    async with session.post(url, json=json, headers=headers) as response:
        content = await response.text()
        return content

async def ai_query(text):
    """Query the AI for stuff"""
    print("query")
    data = {
        "prompt": text,
        "userId": "#/chat/1686026187718",
        "network": True,
        "system": "",
        "withoutContext": False,
        "stream": False
    }
    response = await post("https://api.aichatos.cloud/api/generateStream", json=data)
    return response
    
async def send_message(room, responseText, event_id, compact=True):
    """
    Format a message appropriately to be less spammy. Unfortunately
    plaintext-only clients will face walls of text.
    """
    # so it can't html. sorry ai
    formattedResponse = responseText.replace('<', '&lt').replace('>', '&gt')
    # preserve line breaks
    formattedResponse = responseText.replace('\n', '<br>')
    # decide if <details> should be used to be less spammy
    print("sending a message")
    if compact:
        formattedResponse = f"<details><summary>AI response: </summary>{formattedResponse}</details>"
    else:
        formattedResponse = responseText
            
    await client.room_send(
        room_id=room.room_id,
        message_type="m.room.message",
        content={
            "msgtype": "m.text",
            "format": "org.matrix.custom.html",
            "body": responseText,
            "formatted_body": formattedResponse,
            "m.relates_to": {
                "m.in_reply_to": {
                    "event_id": event_id
                }
            }
        }
    )   

async def handle_message(room, event):
    """See if the message is a command. If so, send the content to the AI"""
    try:
        if event.body.startswith('!ai'):
            print("!ai used")
            parseIndex = event.body.index("!ai")
            responseText = await ai_query(event.body[parseIndex + 4:])
            await send_message(room, responseText, event.event_id)
        
    except Exception as e:
        print(e)
        await send_message(room, "error", event.event_id, False)
        
async def message_cb(room, event):
    """
    Create a background task for handle_message, so we
    can ask about pee rockets twice as fast
    """
    task = asyncio.create_task(handle_message(room,event))
    
async def main():
    global session # post() uses the session
    session = aiohttp.ClientSession()
    client.access_token = logindata["token"]
    # await client.login()
    print("initial sync")
    await client.sync(timeout=5000)
    print("synced, waiting for messages")
    client.add_event_callback(message_cb, RoomMessageText)
    await client.sync_forever(timeout=30000)

asyncio.get_event_loop().run_until_complete(main())
